import random
import os

def responses(input_text):
    user_message = str(input_text).lower()
    curse_words = ["даня", "лопата", "лопатич", "лопатенко", "данило", "danya", "dan", "lopata", "shovel"]
    if len(user_message) <= 4:
        return "Вась"
    if "цапар" in user_message:
        return "Артеме, Артеме\nЦапар ніколи не запустить пару"
    elif user_message.find(str(filter(lambda x:x, curse_words))) != -1:
       return "На творця не пиздіти. Тихенько."
    elif user_message[-1] == "?":
        rint = random.randint(1,6)
        if rint == 1:
            return "100 %"
        elif rint == 2:
            return "Скоріше так, ніж ні"
        elif rint == 3:
            return "Можливо"
        elif rint == 4:
            return "Ніколи"
        elif rint == 5:
            return "Скоріше ні, ніж так"
        elif rint == 6:
            return "50 на 50"
    return "Please input a valid question"

imgExtension = ["png", "jpeg", "jpg"] #Image Extensions to be chosen from
allImages = list()

def chooseRandomImage(directory="D:/pymemes/dummybot/files/favouritememes"):
    for img in os.listdir(directory): #Lists all files
        ext = img.split(".")[len(img.split(".")) - 1]
        if (ext in imgExtension):
            allImages.append(img)
    choice = random.randint(0, len(allImages) - 1)
    chosenImage = allImages[choice] #Do Whatever you want with the image file
    return directory + "/" + chosenImage

# memeFileExtension = ["mp4"]
# allMemeFiles = list()
#
# def chooseRandomMemeFile(directory="D:/pymemes/dummybot/files/favouritememes"):
#     for memefile in os.listdir(directory):
#         ext = memefile.split(".")[len(memefile.split(".")) - 1]
#         if (ext in memeFileExtension):
#             allMemeFiles.append(memefile)
#         choice = random.randint(0, len(allImages) - 1)
#         chosenMeme = allMemeFiles[choice]
#         return directory + "/" + chosenMeme

def responseByProcent(input_text):
    user_message = str(input_text).lower()
    if len(user_message) <= 4:
        return "Вась"
    if user_message.__contains__("влад") and user_message.__contains__("гей") and len(user_message) == 12:
        return "Шанс 100%"
    #if user_message.find("Влад") != 1 and user_message.find("гей") != 1:
        #return "Шанс 100 %"
    rint = random.randint(1, 100)
    if user_message[-1] == "?":
        return "Шанс " + str(rint) + "%"
    return "Pls enter a valid question"